package com.maven_test;

import java.util.Scanner;

/**
 * Main program class
 */
public class Main {
    /**
     * This program make several actions like
     * - Use Interval to find odd and even numbers, their sum
     * - Use Fibonacci to find max odd and even Fibonacci numbers
     * and find out percentage of odd and even numbers
     *
     * @param args command arguments
     * @author Andrii Syd
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int max;
        int min;
        int N;

        System.out.println("Please enter an interval (integer numbers), first line - min, second line - max");
        min = scanner.nextInt();
        max = scanner.nextInt();

        Intervals interval = new Intervals(min, max);
        interval.minToMax();
        interval.maxToMin();
        interval.sum();

        System.out.println("Now enter the size of set where I have to find max odd and even Fibonacci numbers: ");
        N = scanner.nextInt();

        Fibonacci f = new Fibonacci(N);
        f.findF1();
        f.findF2();
        f.findPercentage();
    }


}
