package com.maven_test;

/**
 * Prints intervals of odd and even numbers their sum
 */
public class Intervals {
    private static int i = 0;
    private int min;
    private int max;

    /**
     * Class constructor to initialize min and maz values
     *
     * @param minC start value
     * @param maxC end value
     */
    public Intervals(int minC, int maxC) {
        min = minC;
        max = maxC;
    }

    /**
     * Prints all odd values from min to max
     */
    public void minToMax() {
        System.out.print("Odd numbers: ");
        for (i = min; i <= max; i++)
            if (i % 2 != 0)
                System.out.print(i + ", ");
    }


    /**
     * Prints all even values from max to min
     */
    public void maxToMin() {
        System.out.print("\nEven numbers: ");
        for (i = max; i >= min; i--)
            if (i % 2 == 0)
                System.out.print(i + ", ");
    }

    /**
     * Finds sum of all values in interval from min to max
     */
    public void sum() {
        System.out.print("\nSum: ");
        int sum = 0;
        for (i = min; i <= max; i++)
            sum += i;
        System.out.println(sum);
    }

}
