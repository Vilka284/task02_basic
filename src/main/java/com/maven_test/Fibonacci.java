package com.maven_test;


/**
 * Finds max odd and even Fibonacci numbers and percentage of Fibonacci's
 */
public class Fibonacci {
    private int percentageOdd = 0;
    private int percentageEven = 0;
    private int nOfFibonacci = 0;
    private int N;

    /**
     * Class constructor to initialize N - max value in set of fibonacci numbers
     *
     * @param NC
     */
    Fibonacci(int NC) {
        N = NC;
    }

    /**
     * Finds max odd Fibonacci
     */
    public void findF1() {
        int F1 = 0;
        F1 = findFibonacci(N);
        int i = 0;
        while (true) {
            if (F1 % 2 != 0) {
                System.out.println("The biggest odd Fibonacci: " + F1);
                break;
            } else {
                i++;
                F1 = findFibonacci(N - i);
            }
        }
    }

    /**
     * Finds max even Fibonacci
     */
    public void findF2() {
        int F2 = 0;
        F2 = findFibonacci(N);
        int i = 0;
        while (true) {
            if (F2 % 2 == 0) {
                System.out.println("The biggest even Fibonacci: " + F2);
                break;
            } else {
                i++;
                F2 = findFibonacci(N - i);
            }
        }
    }

    /**
     * Find percentage of both even and odd fibonacci
     */
    public void findPercentage() {
        findFibonacci(N);
        System.out.println(nOfFibonacci);
        System.out.println("percentage of odd Fibonacci: " + ((double) (percentageOdd) / (double) (nOfFibonacci)) * 100 + " %");
        System.out.println("percentage of even Fibonacci: " + ((double) (percentageEven) / (double) (nOfFibonacci)) * 100 + " %");
    }


    /**
     * @param n
     * @return fibonacci number
     */
    private int findFibonacci(int n) {
        nOfFibonacci = 0;
        percentageEven = 0;
        percentageOdd = 0;
        int fib;
        int fib1 = 0;
        int fib2 = 1;

        while (true) {
            nOfFibonacci++;
            fib = fib1;
            if (fib + fib2 > n)
                break;
            fib1 = fib + fib2;
            fib2 = fib;
            if (fib % 2 == 0)
                percentageEven++;
            else
                percentageOdd++;
        }
        return fib;
    }
}
